Infos s. https://plantuml.com/de-dark/salt

~~~plantuml
@startsalt
{+
{* File | Edit | Source | Refactor
 Refactor | New | Open File | - | Close | Close All }
{/ General | Fullscreen | Behavior | Saving }
{
{ Open image in: | ^Smart Mode^ }
[X] Smooth images when zoomed
[X] Confirm image deletion
[ ] Show hidden images
}
[Close]
}
@endsalt
~~~

Editor und Vorschau gibt es direkt in gitlab oder unter http://www.plantuml.com/