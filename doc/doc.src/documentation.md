# Projektitel

## Allgemeines

![Quelle: https://i.pinimg.com/564x/f8/ed/3b/f8ed3b044157ce90d7de3f8d80558fed.jpg](assets/documentation-2024-04-11.png)


### Projektmitarbeiter

| Name          | Tätigkeiten                           |
| ------------- | ------------------------------------- |
| Mitarbeiter 1 | Projektsteuerung und -koordination    |
| Mitarbeiter 2 | OOP Design + Umsetzung, Dokumentation |
| Mitarbeiter 3 | GUI Design, Umsetzung, Testing        |

### Kurze Projektbeschreibung

Welche Ziele verfolgt dieses Projekt? Welche Möglichkeiten, Fähigkeiten, Arbeitsersparnisse, etc. bietet das Projekt

### Theoretische Grundlagen

Was ist notwendig für die Umsetzung? Mathematische, physikalische Grundlagen? Welche Daten werden erhoben, wie verarbeitet, wie ausgegeben, wie gespeichert, wie übertragen?

## Umsetzung

Hier kommt eine graphische Darstellung und/oder verbale Erklärung, wie die Anwendung aufgebaut ist. Mögliche Darstellungen sind Fluss- bzw. Aktivitätsdiagramme, Klassendiagramme, Use Case Diagramme, Statemachines, etc.

Aus welchen Bestandteilen besteht das Projekt? Hier erfolgt die verbale Beschreibung und eine graphische Darstellung.

Graphische Darstellungen lassen sich gut mit `plantuml` erzeugen (z.B. https://www.planttext.com/, in Obsidian mit dem dem plantuml plugin).

### Use Case Diagramm

~~~plantuml
@startuml
left to right direction
actor "Food Critic" as fc
rectangle Restaurant {
  usecase "Eat Food" as UC1
  usecase "Pay for Food" as UC2
  usecase "Drink" as UC3
}
fc --> UC1
fc --> UC2
fc --> UC3
@enduml
~~~

### Komponentendiagramm

~~~plantuml
@startuml

title Sample Application with DB and csv Access

package "Main Application" as APP {
component MainWindow 
component EditItem
MainWindow ..> EditItem : use


}

note bottom of EditItem: Editing of one  \nspecific item

package "Data Access Layer" as DAL {
DB - [sqlite]
CSV - [csv files]
}

package "Data Model" as MODEL {

[location]
[user]
}

@enduml
~~~

### Klassendiagramme

~~~plantuml
@startuml

hide circle
skinparam classAttributeIconSize 0

class Konto{
    -iban: String
    -kontostand: double
    #{static} kontenListe: Konto[0..*]
    +Konto(iban: String)
    +einzahlen(betrag:double):void
    +auszahlen(betrag:double):boolean
    +getKontostand():double
    +kontoStatus():String
    #{static} listeStatusAllerKonten():String
}

Investment <|- Konto
PrivatKonto -left-|> Konto
GeschaeftsKonto -up-|> Konto
VereinsKonto -down-|> Konto
PrivatKonto <|-right- SparKonto
@enduml 
~~~


### Voraussetzungen

Welche Bibliotheken, frameworks, etc. werden eingesetzt? 
- Bezeichnung und Version
- Quellenangabe, URL
- Lizenzierungstyp (z.B. GNU GPL)


## Testläufe

Hier sind Testläufe zu planen, durchzuführen, zu bewerten und zu dokumentieren. Wenn es bei den Tests auch Dateien mit Resultaten gibt, so sind diese ebenso abzugeben.

### Test #1

### Test #2

## Benutzungsanleitung

Hier mit eine Anleitung erstellen, damit die Anwendung für jeden anwendbar ist.

## Programmcode

### Graphische Oberfläche MainWindow.xaml

~~~xml Title="MainWindow" linenums="1"
<Window x:Class="Databinding_Intro.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:Databinding_Intro"
        mc:Ignorable="d"
        Title="MainWindow" Height="450" Width="380">
    <DockPanel>
		<TextBlock Text="Hello World"
    </DockPanel>/>
</Window>
~~~

### Code behind MainWindow.xaml.cs

~~~csharp
using System;
namespace Databinding_Intro
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
    }
}
~~~
