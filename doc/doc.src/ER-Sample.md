Infos s. https://graphviz.org/Gallery/neato/ER.html

~~~plantuml
@startdot
graph ER {

	layout=neato
	overlap = false;

    label = "\n\nEntity Relation Diagram\nMillionenshowKlassen-Lehrer-Schüler";

	node [shape=box]; tblSchueler; tblLehrer; tblKlassen
	
	node [shape=ellipse] 
		{node [label=<<u>Kennzahl</u>>] Kennzahl} 
		{node [label=<<u>Kuerzel</u>>] Kuerzel} 
		{node [label=<<u>Name</u>>] NameKlasse}   	   

	node [shape=ellipse; style=dashed] 
		fKlasse; 
		
	node [shape=ellipse; color=black; style=solid] {node [label=Name] NameSchueler} GebDatum; 
	node [shape=ellipse] {node [label=Name] NameLehrer} Gehalt; 
	node [shape=ellipse]  Raumnummer;

	node [shape=diamond,width=1, height=1] unterrichtet; "gehen in"
	
	tblSchueler -- NameSchueler
	tblSchueler -- GebDatum
	tblSchueler -- fKlasse
	tblSchueler -- Kennzahl;
	
	tblKlassen -- NameKlasse
	tblKlassen -- Raumnummer
	
	tblLehrer -- NameLehrer
	tblLehrer -- Kuerzel
	tblLehrer -- Gehalt
	
	tblSchueler -- "gehen in" [label=1, len=1.5, labeldistance=0] 
	"gehen in" -- tblKlassen [label=n]
	tblLehrer -- unterrichtet [label=m]
	unterrichtet -- tblKlassen [label=n]
}
@enddot
~~~

Editor und Vorschau gibt es direkt in gitlab oder unter http://www.plantuml.com/