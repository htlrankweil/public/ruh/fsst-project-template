# DBI Projekt: Anforderungen

Anbei findet ihr die Anforderungen an das Projekt. 

## Organisatorisches

* Idealerweise 2er Teams
* Arbeit teils im Unterricht, teils zuhause

## Technischer Inhalt

Verpflichtend:

* C# Anwendungen mit grafischer Oberfläche
* Anbindung einer Datenbank (SQLite, MariaDB, etc.)
* SQL Anforderungen
  * Mindestens zwei Tabellen
  * Mindestens eine Abfrage mit Join
  * CRUD Abfragen
    * Erzeugen von Datensätzen
    * Lesen von Datensätzen
    * Update von Datensätzen
    * Löschen von Datensätzen

## Benotung

* Erfüllung der Grundanforderungen => max. 3er möglich
* Einflussfaktoren für bessere Noten
  * Komplexität des Projekts
  * Umsetzung des Projekts (Feinschliff, keine Bugs, ist die Anwendung intuitiv)
  * Gesamteindruck
* Aktive Verwendung von GIT
  * Jedes Gruppenmitglied committed regelmäßig
  * Die commits sind sinnvoll
* Abgaben
  * Pünktlich
  * Vollständig
  * Entsprechend der Anforderung
* Dokumenation
  * Vollständig, lesbar, laufend geführt
  * Markdown Datei(en)

## Ablauf

* Team suchen und Projektidee ausarbeiten
* Planungsphase
* Umsetzungsphase
* Kurzpräsentation und Abgabe

### Team + Projektidee

Jedes Team überlegt sich eine Projektidee. Dabei sollen bereits folgende Überlegungen in einem Dokument zusammengefasst werden.

* Wie werden die Mindestanforderungen umgesetzt?
* Welche Features sind ein muss
* Welche Features sind Erweiterungen (nice-to-have), wenn genügend Zeit bleibt
* Wie möchten wir das Ganze grob umsetzen

Abgabeform ist hierbei ein kurzes Markdown Dokument. Nach der Freigabe könnt ihr mit der nächsten Phase starten.

### Präsentation/Abgabe

Den genauen Ablauf der Präsentationen werden wir kurz vor Ende besprechen. Die Abgabeform ist wie folgt.

* Dokumentation als Markdown (+ PDF Export)
  * Projekttagebuch pro Teammitglied (zB Tabelle)
    * Wer hat wann an was gearbeitet
  * Kurzbeschreibung des Projekts
    * Must-Haves, Nice-To-Haves
  * Abfragen die ihr intern verwendet
  * Kurzanleitung
    * Installationsanleitung
    * Bedienung + Screenshots

* `[GXX]_[Projektname]_[Name1]_[Name2]`
  * `doc` ... Dokumentation (pdf, md)
  * `src` ... Komplettes VisualStudio Projekt
  * `app` ... Kompiliertes Programm (*.exe) mit allen benötigten Abhängigkeiten (Bilder, andere `*.dlls`)

Die Ordnerstruktur für das Projekt `Example Project` kann so aussehen:
~~~plantuml
@startwbs


+ Example Project

++ doc
+++ sourcecode of documentation
++++[#ivory] eg. md, LaTeX, docx
+++ documentation.pdf
++++[#ivory] one file with the full documentation as pdf

++ tests
+++ test#1
++++[#ivory] first test with results
+++ test#2
++++[#ivory] second test with results
+++ ...

++ src
+++[#ivory] eg. the *.sln file
+++ WebController
++++[#ivory] eg. one .net project
+++ GUI
++++[#ivory] eg. one .net project

++ app
+++[#ivory] in this folder is the exe with all neccessary libraries

@endwbs
~~~